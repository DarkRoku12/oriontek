﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OrionTek.Classes;

namespace OrionTek.Interfaces
{
    /// <summary> Base CRUD interface </summary>
    /// <typeparam name="DbEntity"> Must be a class that could be inserted into the target dataset </typeparam>
    public class BaseCRUD<Context, DbEntity> : IDisposable
        where DbEntity : class, new()
        where Context : DbContext, new()
    {
        private Context _Context { get; init; }

        private BaseRepository<Context , DbEntity> _Repository { get; init; }

        public BaseCRUD()
        {
            this._Context = new Context();
            this._Repository = new BaseRepository<Context , DbEntity>( this._Context );
        }

        public virtual IEnumerable<DbEntity> List
        (
            Expression<Func<DbEntity , bool>> filter = null ,
            Func<IQueryable<DbEntity> , IOrderedQueryable<DbEntity>> orderBy = null ,
            IEnumerable<string> include = null 
        )
        {
            return this._Repository.List( filter , orderBy , include );
        }

        public int SaveChanges()
        {
            return this._Context.SaveChanges();
        }

        public virtual void Insert( DbEntity entity )
        {
            this._Repository.Insert( entity );
            this.SaveChanges();
        }

        public virtual void Update( DbEntity entity )
        {
            this._Repository.Update( entity );
            this.SaveChanges();
        }

        public virtual void Delete( DbEntity entity )
        {
            this._Repository.Delete( entity );
            this.SaveChanges();
        }

        public virtual void Dispose()
        {
            this._Context.Dispose();
            GC.SuppressFinalize( this );
        }
    }
}
