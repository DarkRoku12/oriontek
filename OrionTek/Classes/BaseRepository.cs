﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using OrionTek.Interfaces;

namespace OrionTek.Classes
{
    class BaseRepository<Context, DbEntity> : IBaseRepository<DbEntity>
        where DbEntity : class
        where Context : DbContext
    {
        internal Context context;
        internal DbSet<DbEntity> dbSet;

        public BaseRepository( Context context )
        {
            this.context = context;
            this.dbSet = context.Set<DbEntity>();
        }

        public virtual IEnumerable<DbEntity> List
        ( 
            Expression<Func<DbEntity , bool>> filter = null , 
            Func<IQueryable<DbEntity> , IOrderedQueryable<DbEntity>> orderBy = null,
            IEnumerable<string> include = null
        )
        {
            IQueryable<DbEntity> query = dbSet;

            if( filter != null ) // Add filter [if provided].
                query = query.Where( filter );

            if( include != null ) // Include extra navigation properties.
                foreach( var property in include )
                    query = query.Include( property );

            if( orderBy != null ) // Order by [if provided].
                return orderBy( query ).ToList();
            else
                return query.ToList();
        }

        public virtual DbEntity GetById( object id )
        {
            return dbSet.Find( id );
        }

        /// <summary> Add an entry into the context </summary>
        public virtual void Insert( DbEntity entity )
        {
            dbSet.Add( entity );
        }

        /// <summary> Deletes a previously created entry by its key values </summary>
        public virtual void Delete( object id )
        {
            this.Delete( dbSet.Find( id ) );
        }

        /// <summary> Deletes a previously created entry </summary>
        public virtual void Delete( DbEntity entityToDelete )
        {
            // Re-attach the entity is was previosly removed.
            if( context.Entry( entityToDelete ).State == EntityState.Detached )
                dbSet.Attach( entityToDelete );

            // Remove the entity.
            dbSet.Remove( entityToDelete );
        }

        /// <summary> Updates a previously created entry </summary>
        public virtual void Update( DbEntity entityToUpdate )
        {
            dbSet.Attach( entityToUpdate ); // Set the entity as Attached [previosly crearted]
            context.Entry( entityToUpdate ).State = EntityState.Modified; // Mark as dirty.
        }
    }
}
