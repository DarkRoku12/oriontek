﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OrionTek.Interfaces;

namespace OrionTek.Classes
{
    public class OriontekController<DbEntity> : ControllerBase where DbEntity : class, new()
    {
        protected BaseCRUD<OriontekDbContext , DbEntity> _API { get; init; }

        public OriontekController()
        {
            // Initialize the context on every request, given that EntityFramework Context is not thread safe.
            this._API = new BaseCRUD<OriontekDbContext , DbEntity>();
        }

        [HttpGet]
        public virtual IEnumerable<DbEntity> List()
        {
            return this._API.List( orderBy: e => e.OrderBy( i => "Id" ) );
        }

        [HttpPost]
        public virtual JsonResult Insert( DbEntity entity )
        {
            this._API.Insert( entity );
            return new JsonResult( "ok" );
        }

        [HttpPut]
        public virtual JsonResult Update( DbEntity entity )
        {
            this._API.Update( entity );
            return new JsonResult( "ok" );
        }

        [HttpDelete]
        public virtual JsonResult Delete( DbEntity entity )
        {
            this._API.Delete( entity );
            return new JsonResult( "ok" );
        }
    }

    [Produces( "application/json" )]
    public class OriontekJsonController<DbEntity> : OriontekController<DbEntity> where DbEntity : class, new()
    {
        public OriontekJsonController() { }
    }
}
