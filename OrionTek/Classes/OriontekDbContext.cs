﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using OrionTek.Models;

namespace OrionTek.Classes
{
    public class OriontekDbContext : DbContext
    {
        protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder )
        {
            // Get configuration of the database.
            var path = "Databases:OriontekTest";
            var ip = Startup.Configuration[ $"{path}:ip" ];
            var port = Startup.Configuration[ $"{path}:port" ];
            var db = Startup.Configuration[ $"{path}:database" ];
            var user = Startup.Configuration[ $"{path}:user" ];
            var pass = Startup.Configuration[ $"{path}:password" ];

            // Build the connection string.
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = $"{ip},{port}" ,
                InitialCatalog = db ,
                UserID = user ,
                Password = pass ,
                IntegratedSecurity = false
            };

            // Set the connection string as the source of the context.
            optionsBuilder.UseSqlServer( builder.ConnectionString );
        }

        ///// Tables /////
        public DbSet<Company> Companies { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<CompanyClient> CompanyClients { get; set; }
    }
}
