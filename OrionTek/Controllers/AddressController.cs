﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrionTek.Classes;
using OrionTek.Models;

namespace OrionTek.Controllers
{
    [ApiController]
    [Route( "[controller]" )]
    public class AddressController : OriontekController<Address>
    {
    }
}
