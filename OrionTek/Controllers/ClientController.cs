﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrionTek.Classes;
using OrionTek.Models;

namespace OrionTek.Controllers
{
    [ApiController]
    [Route( "[controller]" )]
    public class ClientController : OriontekController<Client>
    {
        [HttpGet]
        public override IEnumerable<Client> List()
        {
            var include = new string[] { "Addresses" };
            return this._API.List( orderBy: e => e.OrderBy( i => i.Id ) , include: include );
        }
    }
}
