﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrionTek.Classes;
using OrionTek.Models;

namespace OrionTek.Controllers
{
    [ApiController]
    [Route( "[controller]" )]
    public class CompanyClientController : OriontekController<CompanyClient>
    {
        [HttpGet]
        public override IEnumerable<CompanyClient> List()
        {
            var include = new string[] { "Company" , "Client" };
            return this._API.List( orderBy: e => e.OrderBy( i => i.Id ) , include: include );
        }
    }
}
