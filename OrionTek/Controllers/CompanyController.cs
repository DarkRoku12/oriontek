﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using OrionTek.Classes;
using OrionTek.Models;

namespace OrionTek.Controllers
{
    [ApiController]
    [Route( "[controller]" )]
    public class CompanyController : OriontekController<Company>
    {
        [HttpGet]
        public override IEnumerable<Company> List()
        {
            var include = new string[] { "CompanyClients" , "CompanyClients.Client" };
            return this._API.List( orderBy: e => e.OrderBy( i => i.Id ) , include: include );
        }
    }
}
