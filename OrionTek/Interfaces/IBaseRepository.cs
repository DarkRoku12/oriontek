﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace OrionTek.Interfaces
{
    /// <summary> Base repository interface </summary>
    /// <typeparam name="DbEntity"> Must be a class that could be inserted into the target dataset </typeparam>
    public interface IBaseRepository<DbEntity> where DbEntity : class
    {
        /// <summary> Delete a database entry </summary>
        void Delete( DbEntity deleting );

        /// <summary> Find a database entry and delete it </summary>
        void Delete( object deleteById );

        /// <summary Get a list of entities matching the provided filter </summary>
        IEnumerable<DbEntity> List
        (
            Expression<Func<DbEntity , bool>> filter = null ,
            Func<IQueryable<DbEntity> , IOrderedQueryable<DbEntity>> orderBy = null ,
            IEnumerable<string> include = null
        );

        /// <summary> Get a single entity by its Id </summary>
        DbEntity GetById( object id );

        /// <summary> Inserts a new entity into the database </summary>
        void Insert( DbEntity entity );

        /// <summary> Updates a new entity into the database </summary>
        void Update( DbEntity entityToUpdate );
    }
}
