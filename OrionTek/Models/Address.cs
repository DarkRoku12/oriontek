﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace OrionTek.Models
{
    public class Address
    {
        public long Id { get; set; }

        [StringLength( 60 )]
        public string City { get; set; }

        [StringLength( 60 )]
        public string State { get; set; }

        [StringLength( 300 )]
        public string Street { get; set; }

        /// <summary> Navigation property to Client </summary>
        public long ClientId { get; set; }

        [JsonIgnore]
        public virtual Client Client { get; set; }
    }
}
