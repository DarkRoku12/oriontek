﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace OrionTek.Models
{
    public class Client
    {
        public long Id { get; set; }

        public string Name { get; set; }

        /// <summary> Navigation property to Company </summary>
        /// <summary> A Company can have multiple Clients </summary>
        [JsonIgnore]
        public ICollection<CompanyClient> CompanyClients { get; set; }

        /// <summary> A Client can have multiple Address </summary>
        public ICollection<Address> Addresses { get; set; }
    }
}
