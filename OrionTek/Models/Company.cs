﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace OrionTek.Models
{
    public class Company
    {
        public long Id { get; set; }

        public string Name { get; set; }

        /// <summary> A Company can have multiple Clients </summary>
        public ICollection<CompanyClient> CompanyClients { get; set; }
    }
}
