﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;

namespace OrionTek.Models
{
    // Unique index for (CompanyId,ClientId).
    [Index( nameof( CompanyId ) , nameof( ClientId ) , Name = "UIDX_CompanyClient" , IsUnique = true )]
    public class CompanyClient
    {
        public long Id { get; set; }

        public long CompanyId { get; set; }

        public long ClientId { get; set; }

        [JsonIgnore]
        public Company Company { get; set; }

        public Client Client { get; set; }
    }
}
