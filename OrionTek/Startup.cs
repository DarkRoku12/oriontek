using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OrionTek.Classes;

namespace OrionTek
{
    public class Startup
    {
        private static IConfiguration _Configuration { get; set; }

        public static IConfiguration Configuration { get => _Configuration; }

        public Startup( IConfiguration configuration )
        {
            _Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices( IServiceCollection services )
        {
            //services.AddDbContext<MyAppContext>(
            //    options => options.UseSqlServer( @"Data Source=darkroku12.ovh,8100;Database=OriontekTest;Integrated Security=false;User ID=oriontek;Password=otk123;" )
            //);

            services.AddDbContext<OriontekDbContext>();

            services.AddControllers();
            services.AddSwaggerGen( c =>
             {
                 c.SwaggerDoc( "v1" , new OpenApiInfo { Title = "OrionTek" , Version = "v1" } );
             } );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app , IWebHostEnvironment env )
        {
            using( var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope() )
            {
                using var context = serviceScope.ServiceProvider.GetService<OriontekDbContext>();
                context.Database.Migrate();
            }

            if( env.IsDevelopment() )
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI( c => c.SwaggerEndpoint( "/swagger/v1/swagger.json" , "OrionTek v1" ) );
            }

            app.UseCors( policy =>
            {
                policy.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
            } );

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints( endpoints =>
             {
                 endpoints.MapControllers();
             } );
        }
    }
}
