## Oriontek - proyecto de prueba.

## Autor: Enmanuel Reynoso - 03/2021

## Correr el proyecto:
1 - Compilar el proyecto con Visual Studio (utilizando la solucion OrionTek.sln) y ejecutarlo.

1.1 - Alternativamente se podría utilizar ```dotnet build + dotnet run```.

2 - Initializar el servidor del client-side para correr la App de VueJS

2.1 - Comando: ```cd public & npm install & npm run serve```

## Oportunidades de mejoras:

### Back-end
- Utilizar DTOs para enviar y recibir datos más específicos.
- Exponer el contexto interno para los controladores, en caso de que se necesiten una API más abundante.
- Manejar los errores y/o excepciones y proveer mensajes descriptivos.
- Validar los datos provistos por el front-end.

### Front-end
- Utilizar solo una librería de CSS (actualmente se utiliza vuematerial.io y vuikit)
- Mejorar y estandarizar las convenciones en los nombres.
- Desacomplar la aplicación en más componentes.
- Proveer validaciones de los datos ingresados.
- Implementar el borrado de registros (Manejar también los conflictos de claves foráneas)
- Implementar Vuex para una mejor manejo del estado de la SPA.

## Notas:

1) La base de datos esta en un servidor privado, para proyectos personales 
(dado que no me gusta tener instancias de bases de datos corriendo en mi máquina local)
para fines de esta prueba esta configurada una base de datos remota, con un usuario hábil para consumirla.

2) Si se desea instalar la base de datos de forma local, solo se tiene que especificar en el ```OrionTek/appsettings.json```
y cambiar las preferencias de conexión. Al ejecutar el projecto, las migraciones de EntityFramework correrán de forma automática.

3) Usualemente, me gusta tener el control de la base de datos, así que no suelo dejarle las migraciones a EF ya que prefiero hacer mi base de datos a mano, en este caso, utilizé las migraciones por simplicidad y demostración de conocimientos.

4) Cualquier duda, pregunta, o sugerencia, no duden en contactarme, gracias.