
/// Standard modules & plugins ///
import Vue from "vue";

/// Vue Material
import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";

/// View UIKit
import Vuikit from "vuikit"
import VuikitIcons from "@vuikit/icons"
import "@vuikit/theme"

/// Projects components ///
import Main from "./Main.vue";
import Router from "./common/Router.js";

/// Final configuration ///
Vue.config.productionTip = false;

Vue.use( VueMaterial );
Vue.use( Vuikit );

let vm = new Vue(
{
  router : Router ,
  render : h => h( Main )
}).$mount( "#oriontek-entry" );


