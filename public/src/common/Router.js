import Vue from "vue";
import VueRouter from "vue-router";

// Routes
import BodyView from "../views/Body.vue";
import CompanyView from "@/views/Company.vue";
import ClientView from "@/views/Client.vue";

const routes = 
[
  {
    path: "/",
    name: "Root" ,
    redirect: { name: "Body|Company" }
  },

  {
    path: "/Body",
    name: "Body" ,
    component: BodyView,
    children: 
    [
      {
        name: "Body|Company",
        path: "Company" ,
        component: CompanyView
      },
      {
        name: "Body|Client",
        path: "Client" ,
        component: ClientView
      }
    ]
  }
];

Vue.use( VueRouter );

const Router = new VueRouter( { routes } );

export default Router
