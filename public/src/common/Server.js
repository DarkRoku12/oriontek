
import Axios from "axios" ;

const instance = Axios.create(
{
  baseURL: "https://localhost:44390/",
});

export default instance;